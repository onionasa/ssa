library(RODBC)
library(rpart)
library(RWeka)
library(ipred)
library(randomForest)
library(gbm)
library(C50)
library(MASS)
library(e1071)
library(party)
library(class)


sdmt.installPackages <- function(){
  install.packages("rJava")
  install.packages("RWekajars")
  install.packages("RODBC")
  install.packages("rpart")
  install.packages("RWeka")
  install.packages("ipred")
  install.packages("randomForest")
  install.packages("gbm")
  install.packages("C50")
  install.packages("MASS")
  install.packages("e1071")
  install.packages("party")
  install.packages("class")

}

sdmt.DTCLASS = "dt-class"
sdmt.DTREG = "dt-reg"
sdmt.DTC45 = "dt-c45"
sdmt.DTC50 = "dt-c50"
sdmt.DTBAG = "dt-bagging"
sdmt.DTGBM = "dt-gbm"
sdmt.DTPART = "dt-part"
sdmt.DTRF = "dt-rf"
sdmt.NB = "nb"
sdmt.KNN = "knn"


sdmt.createConnection <- function(Connection=NULL){
  if(!is.null(Connection)){
    dbcon = odbcConnect(Connection)
    if(dbcon!=-1){
      print("Connection created")
      return(dbcon)
    }else{
      print("Connection failed")
      return(-1)
    }
  }else{
    print("Connection not defined")
    return(-1)
  }

}


sdmt.dbQuery <- function(Connection, Query){
  return(sqlQuery(Connection,Query))
}


sdmt.dbStorePredictions <- function(Connection, TestData, Prediction, TableName){
  dbResult <- cbind(TestData,Prediction)
  x <- data.frame(dbResult,row.names = NULL)
  sqlDrop(Connection,TableName)
  sqlSave(Connection, dat = x, TableName, append = FALSE, rownames = FALSE)
}


sdmt.classifier <- function(datamodel = NULL, traindata, testdata, clstype="dt", nn = 3, cv = NULL){
  #Variables
  temp = list()

  if (clstype == "dt-class"){
    ########## RPART DECISION TREE  #########
    # Preparing Decision Tree - Classification
    temp$DecisionTree = rpart(datamodel, data = traindata, method = "class")
    # Pruning Decision Tree By Minimum Cross Validation Error
    #temp$PrunedDecisionTree = prune(temp$DecisionTree, cp = temp$DecisionTree$cptable[which.min(temp$DecisionTree$cptable[,"xerror"]),"CP"])
    # Predicting Test Data Using Pruned Decision Tree
    temp$PredictionOnTest = predict(temp$DecisionTree, testdata)
    temp$PredictionOnTestClasses <- colnames(temp$PredictionOnTest)[max.col(temp$PredictionOnTest, ties.method = c("random"))]
    #################################################
  }else  if (clstype == "dt-reg"){
    ########## RPART DECISION TREE  #########
    # Preparing Decision Tree - Regression
    temp$DecisionTree = rpart(datamodel, data = traindata, method = "anova")
    # Pruning Decision Tree By Minimum Cross Validation Error
    temp$PrunedDecisionTree = prune(temp$DecisionTree, cp = temp$DecisionTree$cptable[which.min(temp$DecisionTree$cptable[,"xerror"]),"CP"])
    # Predicting Test Data Using Pruned Decision Tree
    temp$PredictionOnTest = predict(temp$PrunedDecisionTree, testdata)
    temp$PredictionOnTestClasses <- colnames(temp$PredictionOnTest)[max.col(temp$PredictionOnTest, ties.method = c("random"))]
    #################################################
  }else if(clstype == "dt-c45"){
    # Preparing Decision Tree - C4.5
    temp$DecisionTree = J48(datamodel, traindata)
    # Predicting Test Data Using C4.5 - http://stackoverflow.com/questions/12872699/error-unable-to-load-installed-packages-just-now
    temp$PredictionOnTestClasses = predict(temp$DecisionTree, testdata)
  }else if(clstype == "dt-rf"){
    # Preparing Decision Tree - Random Forest
    temp$DecisionTree = randomForest(datamodel, traindata)
    # Predicting Test Data Using Random Forest
    temp$PredictionOnTestClasses = predict(temp$DecisionTree, testdata)
  }else if(clstype == "dt-part"){
    # Preparing Decision Tree - Part - http://stackoverflow.com/questions/12872699/error-unable-to-load-installed-packages-just-now
    temp$DecisionTree = PART(datamodel, traindata)
    # Predicting Test Data Using Part
    temp$PredictionOnTestClasses = predict(temp$DecisionTree, testdata)
  }else if(clstype == "dt-c50"){
    # Preparing Decision Tree - C5.0
    temp$DecisionTree = C5.0(datamodel, traindata,trials=10)
    # Predicting Test Data Using C5.0
    temp$PredictionOnTestClasses = predict(temp$DecisionTree, testdata)
  }else if(clstype == "dt-bagging"){
    # Preparing Decision Tree - Bagging
    temp$DecisionTree = bagging(datamodel, traindata)
    # Predicting Test Data Using Bagging
    temp$PredictionOnTestClasses = predict(temp$DecisionTree, testdata)
  }else if(clstype == "dt-gbm"){
    # Preparing Decision Tree - gbm
    temp$DecisionTree = gbm(datamodel, traindata, distribution="multinomial")
    # Predicting Test Data Using gbm
    temp$PredictionOnTest = predict.gbm(temp$DecisionTree, testdata, n.trees = temp$DecisionTree$n.trees)
    temp$PredictionOnTestClasses = colnames(temp$PredictionOnTest)[max.col(data.frame(temp$PredictionOnTest), ties.method = c("random"))]
  }else if(clstype == "nb"){
    # Preparing Naive Bayes
    temp$NaiveBayes = naiveBayes(datamodel, traindata)
    # Predicting Test Data Using Naive Bayes
    temp$PredictionOnTestClasses = predict(temp$NaiveBayes, testdata)
  }else if(clstype == "knn"){
    # Predicting Test Data Using KNN
    temp$PredictionOnTestClasses = knn(train = traindata, test = testdata, cl = cv, k = nn, prob = TRUE)
  }
  temp$Clstype = clstype
  return(temp)
}

sdmt.increasedPrediction <- function(data_model = NULL, traindata, testdata, clstype="dt", nn = 3, cv = NULL, count = 100){
  len <- nrow(traindata)
  i <- count
  exit <- FALSE
  result <- NULL
  result_index <- NULL
  while(i <= len){
    train <- traindata[1:i,]
    if(clstype == "knn"){
      clsfrResult = sdmt.classifier(datamodel = data_model, traindata = train, testdata = testdata[,-grep(as.character(data_model[2]),colnames(testdata))], clstype = clstype, nn = nn, cv = cv[1:i])
      clsfrResult$MEonTest = mean(testdata[as.character(data_model[2])] != as.character(clsfrResult$PredictionOnTestClasses))
    }else{
      clsfrResult = sdmt.classifier(datamodel = data_model, traindata = train, testdata = testdata, clstype = clstype, nn = nn, cv = cv[1:i])
      clsfrResult$MEonTest = mean(testdata[as.character(data_model[2])] != as.character(clsfrResult$PredictionOnTestClasses))
    }

    result = rbind(result, clsfrResult$MEonTest)
    result_index = rbind(result_index,i)
    i<-i+count
    if(i > len && !exit){
      i=len
      exit = TRUE
    }
  }

  plot(result_index,result*100,type="l", main=paste("Incremental Usage of ",clsfrResult$Clstype," Algorithm on Test Data"),xlab = "Train Count", ylab = "Misclassification Error Rate (%)")
  grid(10, 10, lwd = 2)
}


sdmt.algrtmComparison <- function(data_model = NULL, traindata, testdata, clstypelist=NULL, nn = 3, cv = NULL){
  results = NULL
  if(('dt-class' %in% clstypelist)){
    clsfrResult = sdmt.classifier(datamodel = data_model, traindata = traindata, testdata = testdata, clstype = "dt-class", nn = nn, cv = cv)
    clsfrResult$MEonTest = mean(testdata[as.character(data_model[2])] != as.character(clsfrResult$PredictionOnTestClasses))*100
    results = rbind(results,c(clsfrResult$MEonTest,clsfrResult$Clstype))
  }
  if(('dt-c45' %in% clstypelist)){
  clsfrResult = sdmt.classifier(datamodel = data_model, traindata = traindata, testdata = testdata, clstype = "dt-c45", nn = nn, cv = cv)
  clsfrResult$MEonTest = mean(testdata[as.character(data_model[2])] != as.character(clsfrResult$PredictionOnTestClasses))*100
  results = rbind(results,c(clsfrResult$MEonTest,clsfrResult$Clstype))
  }
  if(('dt-c50' %in% clstypelist)){
  clsfrResult = sdmt.classifier(datamodel = data_model, traindata = traindata, testdata = testdata, clstype = "dt-c50", nn = nn, cv = cv)
  clsfrResult$MEonTest = mean(testdata[as.character(data_model[2])] != as.character(clsfrResult$PredictionOnTestClasses))*100
  results = rbind(results,c(clsfrResult$MEonTest,clsfrResult$Clstype))
  }
  if(('dt-part' %in% clstypelist)){
  clsfrResult = sdmt.classifier(datamodel = data_model, traindata = traindata, testdata = testdata, clstype = "dt-part", nn = nn, cv = cv)
  clsfrResult$MEonTest = mean(testdata[as.character(data_model[2])] != as.character(clsfrResult$PredictionOnTestClasses))*100
  results = rbind(results,c(clsfrResult$MEonTest,clsfrResult$Clstype))
  }
  if(('dt-bagging' %in% clstypelist)){
  clsfrResult = sdmt.classifier(datamodel = data_model, traindata = traindata, testdata = testdata, clstype = "dt-bagging", nn = nn, cv = cv)
  clsfrResult$MEonTest = mean(testdata[as.character(data_model[2])] != as.character(clsfrResult$PredictionOnTestClasses))*100
  results = rbind(results,c(clsfrResult$MEonTest,clsfrResult$Clstype))
  }
  if(('dt-rf' %in% clstypelist)){
  clsfrResult = sdmt.classifier(datamodel = data_model, traindata = traindata, testdata = testdata, clstype = "dt-rf", nn = nn, cv = cv)
  clsfrResult$MEonTest = mean(testdata[as.character(data_model[2])] != as.character(clsfrResult$PredictionOnTestClasses))*100
  results = rbind(results,c(clsfrResult$MEonTest,clsfrResult$Clstype))
  }
  if(('dt-gbm' %in% clstypelist)){
    clsfrResult = sdmt.classifier(datamodel = data_model, traindata = traindata, testdata = testdata, clstype = "dt-gbm", nn = nn, cv = cv)
    clsfrResult$MEonTest = mean(testdata[as.character(data_model[2])] != as.character(clsfrResult$PredictionOnTestClasses))*100
    results = rbind(results,c(clsfrResult$MEonTest,clsfrResult$Clstype))
  }
  if(('nb' %in% clstypelist)){
  clsfrResult = sdmt.classifier(datamodel = data_model, traindata = traindata, testdata = testdata, clstype = "nb", nn = nn, cv = cv)
  clsfrResult$MEonTest = mean(testdata[as.character(data_model[2])] != as.character(clsfrResult$PredictionOnTestClasses))*100
  results = rbind(results,c(clsfrResult$MEonTest,clsfrResult$Clstype))
  }
  if(('knn' %in% clstypelist)){
  cl_index <- grep(as.character(data_model[2]),colnames(testdata))
  clsfrResult = sdmt.classifier(datamodel = data_model, traindata = traindata[,-cl_index], testdata =  testdata[,-cl_index], clstype = "knn", nn = nn, cv = cv)
  clsfrResult$MEonTest = mean(testdata[as.character(data_model[2])] != as.character(clsfrResult$PredictionOnTestClasses))*100
  results = rbind(results,c(clsfrResult$MEonTest,clsfrResult$Clstype))
  }
  results = results[order(-as.integer(results[,1])),]
  barplot(as.integer(results[,1]),names.arg = results[,2],xlab = "Algorithms",ylab = "Misclassification Error Rate (%)",main = paste("Algorithm Comparison"),col = heat.colors(8))
}
